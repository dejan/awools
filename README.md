AWOOLS - AWs tOOLS
==================

Various tools that you may find useful while working with AWS services.

assume-role
-----------

A simple, BASH script that only depends on the 
[AWS CLI](https://docs.aws.amazon.com/cli/) tool. - Very often you need 
to assume particular role. Typically we have a profile
in `./aws/config` and we use it with AWS client. One of the annoying things
is that it will keep asking for MFA whenever you want to 
execute something...

The solution is to use the assume-role script and export AWS_* variables that are recognised by default by all AWS tools.

Examples:

Assume role specified in your AWS profile:

    eval `assume-role 1h2m3s my_aws_profile`

Assume role given a full ARN:

    eval `assume-role 1h2m3s arn:aws:iam::123456789012:role/my-aws-role`

### TODO

- Caching. If you call assume-role with the same profile, yet the session
  is not yet expired, there is no need to create session again. Use the cached data (stored in a file in .local/assume-role most likely)
- At the moment second option, to pass full ARN is not possible. It will be implemented soon.
- Make necessary changes so assume-role can be a BASH alias. So we can have `alias assume-role='. assume-role`

### Similar tools

- https://github.com/remind101/assume-role
- https://github.com/trek10inc/awsume

aws-aliases
-----------

Contains useful BASH aliases such as `aws_whoami` and `aws_clear`.

renew-credentials
-----------------

Useful script to renew your AWS credentials if they are soon to be expired.
